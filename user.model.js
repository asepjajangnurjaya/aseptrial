const sequelize = require('./sequelize')
const {
    Sequelize,
    DataTypes
} = require('sequelize')

const User = sequelize.define('cobaUser', {
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastName: {
        type: DataTypes.STRING
    }
})

module.exports = User