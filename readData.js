const user = require('./user.model')

//tipe function biasa
async function readAlldata() {
    var all = await user.findAll()
    console.log(all)
    return all
}

readAlldata() // cara panggil fucntion biasa

//tipe arrow function
var read = async () => {
    const all = await user.findAll()
    //console.log(all)
    return all

}
read.call() //cara panggil arrow function